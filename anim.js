

function modifValues(){

    var val = $('#progress progress').attr('value'); //prend la valeur de la progress bar 
    if (val>=1){
        $('#comment').text("C'est parti!"); //rajoute du contenu dans comment
    } 
    if (val>=15){
        $('#comment').text("Bon début!");
    } 
    if (val>=30){
        $('#comment').text("Pas mal!");
    } 
    if (val>=50){
        $('#comment').text("La moitié du chemin!");
    } 
    if (val>=70){
        $('#comment').text("Le plus dur est fait!");
    } 
    if (val>=85){
        $('#comment').text("Vous voyez le bout du tunnel!");
    } 
    if(val>=100){
        $('#comment').text("You're feeling good!");
        $('#music').play();
        clearInterval(g); //arrête le setInterval
    }

    var newVal = Number(val)+1; //définie la variable val en int et lui rajoute 0,25
    var txt = Math.floor(newVal)+'%';   //concatène l'entier inférieur de newVal et le %   
      
    $('#progress progress').attr('value',newVal); //donne à l'attribut 'value' la valeur newVal
    $('#progress strong').text(txt); //change le contenu du strong
}


var g= setInterval(modifValues,200); //Appelle une fonction de manière répétée, avec un certain délai fixé entre chaque appel.



//val=30, var undefined car script dans le head, lecteur audio html bloqué, test dom=>jquery
//pb concaténation=> string to number
